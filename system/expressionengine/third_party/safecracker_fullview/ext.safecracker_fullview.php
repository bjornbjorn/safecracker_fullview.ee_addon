<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ExpressionEngine - by EllisLab
 *
 * @package		ExpressionEngine
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2003 - 2011, EllisLab, Inc.
 * @license		http://expressionengine.com/user_guide/license.html
 * @link		http://expressionengine.com
 * @since		Version 2.0
 * @filesource
 */
 
// ------------------------------------------------------------------------

/**
 * SafeCracker Fullview Extension
 *
 * @package		ExpressionEngine
 * @subpackage	Addons
 * @category	Extension
 * @author		Bjørn Børresen
 * @link		http://www.lastfriday.no
 */

class Safecracker_fullview_ext {
	
	public $settings 		= array();
	public $description		= 'SafeCracker File thumbnail - display full image on mouseover!';
	public $docs_url		= 'http://www.wedoaddons.com';
	public $name			= 'SafeCracker Fullview';
	public $settings_exist	= 'n';
	public $version			= '1.0';
	
	private $EE;
	
	/**
	 * Constructor
	 *
	 * @param 	mixed	Settings array or empty string if none exist.
	 */
	public function __construct($settings = '')
	{
		$this->EE =& get_instance();
		$this->settings = $settings;
	}// ----------------------------------------------------------------------
	
	/**
	 * Activate Extension
	 *
	 * This function enters the extension into the exp_extensions table
	 *
	 * @see http://codeigniter.com/user_guide/database/index.html for
	 * more information on the db class.
	 *
	 * @return void
	 */
	public function activate_extension()
	{
		// Setup custom settings in this array.
		$this->settings = array();

        $hooks = array(
     			'cp_js_end'
        );

        foreach ($hooks as $hook)
        {
            $data = array(
                'class'		=> __CLASS__,
                'method'	=> 'on_'.$hook,
                'hook'		=> $hook,
                'settings'	=> serialize($this->settings),
                'version'	=> $this->version,
                'enabled'	=> 'y'
            );

            $this->EE->db->insert('extensions', $data);
        }
	}	

	// ----------------------------------------------------------------------

	/**
	 * Disable Extension
	 *
	 * This method removes information from the exp_extensions table
	 *
	 * @return void
	 */
	function disable_extension()
	{
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->delete('extensions');
	}

	// ----------------------------------------------------------------------

	/**
	 * Update Extension
	 *
	 * This function performs any necessary db updates when the extension
	 * page is visited
	 *
	 * @return 	mixed	void on update / false if none
	 */
	function update_extension($current = '')
	{
		if ($current == '' OR $current == $this->version)
		{
			return FALSE;
		}
	}	
	


    public function on_cp_js_end($data)
    {

        $out_js = "

        $('.safecracker_file_thumb').each(function(e) {
            var the_image = $(this).children('img');

            var safecracker_thumbs_original_src = new Array();

            the_image.mouseover(function(e) {
                safecracker_thumbs_original_src[ $(this).attr('id') ] = $(this).attr('src');
                $(this).attr('src', $(this).attr('src').replace('_thumbs/',''));
            });

            the_image.mouseout(function(e) {
                var thumb_src = safecracker_thumbs_original_src[ $(this).attr('id') ]
                $(this).attr('src', thumb_src);
            });
        });";

        return $this->EE->extensions->last_call.$out_js;
    }


}

/* End of file ext.safecracker_fullview.php */
/* Location: /system/expressionengine/third_party/safecracker_fullview/ext.safecracker_fullview.php */